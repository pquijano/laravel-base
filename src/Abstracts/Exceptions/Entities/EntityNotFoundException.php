<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Exceptions\Entities;

abstract class EntityNotFoundException extends \Exception
{
    public function __construct(string $message = '', $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
