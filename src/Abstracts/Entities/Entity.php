<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Entities;

use Spatie\LaravelData\Optional;

abstract class Entity extends Timestamp
{
    public string|Optional $id;
}
