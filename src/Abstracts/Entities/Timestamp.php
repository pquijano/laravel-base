<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Entities;

use DateTimeInterface;
use Spatie\LaravelData\Data;

abstract class Timestamp extends Data
{
    public ?DateTimeInterface $createdAt;

    public ?DateTimeInterface $updatedAt;
}
