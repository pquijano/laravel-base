<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Models;

use Illuminate\Database\Eloquent\Concerns\HasUlids as HasUlidsTrait;
use Illuminate\Database\Eloquent\Model as AbstractModel;

abstract class Model extends AbstractModel
{
    use HasUlidsTrait;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The data type of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';
}
