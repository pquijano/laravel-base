<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Models;

use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Auth\MustVerifyEmail as MustVerifyEmailTrait;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableInterface;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableInterface;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordInterface;
use Illuminate\Foundation\Auth\Access\Authorizable as AuthorizableTrait;
use PQuijano\LaravelBase\Abstracts\Models\Model as AbstractModel;

abstract class User extends AbstractModel implements AuthenticatableInterface, AuthorizableInterface, CanResetPasswordInterface
{
    use AuthenticatableTrait, AuthorizableTrait, CanResetPasswordTrait, MustVerifyEmailTrait;
}
