<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Models;

use Illuminate\Database\Eloquent\Relations\Pivot as AbstractPivot;

abstract class Pivot extends AbstractPivot
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.u';
}
