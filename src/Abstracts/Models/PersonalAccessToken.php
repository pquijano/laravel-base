<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Models;

use Illuminate\Database\Eloquent\Concerns\HasUlids as HasUlidsTrait;
use Laravel\Sanctum\PersonalAccessToken as AbstractPersonalAccessToken;

abstract class PersonalAccessToken extends AbstractPersonalAccessToken
{
    use HasUlidsTrait;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * The data type of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';
}
