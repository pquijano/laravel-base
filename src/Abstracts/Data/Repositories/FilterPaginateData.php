<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Data\Repositories;

use Spatie\LaravelData\Data;

abstract class FilterPaginateData extends Data
{
    public ?int $page = 1;

    public string $pageName = 'page';

    public ?int $perPage = 50;
}
