<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Abstracts\Data\Repositories;

use Spatie\LaravelData\Data;

abstract class FilterGetData extends Data
{
    public ?int $limit = 100;
}
