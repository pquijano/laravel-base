<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase;

use Illuminate\Support\ServiceProvider as AbstractServiceProvider;
use Illuminate\Support\Str;
use PQuijano\LaravelBase\Mixins\StrMixin;

final class LaravelBaseServiceProvider extends AbstractServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Str::mixin(new StrMixin());
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }
}
