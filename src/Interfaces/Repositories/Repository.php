<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Interfaces\Repositories;

/**
 * @template TEntity of \PQuijano\LaravelBase\Abstracts\Entities\Entity
 * @template TEntityNotFoundException of \PQuijano\LaravelBase\Abstracts\Exceptions\Entities\EntityNotFoundException
 * @template TFilterGetData of \PQuijano\LaravelBase\Abstracts\Data\Repositories\FilterGetData
 * @template TFilterPaginateData of \PQuijano\LaravelBase\Abstracts\Data\Repositories\FilterPaginateData
 */
interface Repository
{
    /**
     * @param  TEntity  $entity
     * @return TEntity
     */
    public function create($entity);

    /**
     * @param  string|int  $id
     * @return bool|null
     *
     * @throws TEntityNotFoundException
     */
    public function delete($id);

    /**
     * @param  string|int  $id
     * @return TEntity
     *
     * @throws TEntityNotFoundException
     */
    public function find($id);

    /**
     * @param  TFilterGetData  $filter
     * @return \Spatie\LaravelData\DataCollection<TEntity>
     */
    public function get($filter);

    /**
     * @param  TFilterPaginateData  $filter
     * @return \Spatie\LaravelData\PaginatedDataCollection<TEntity>
     */
    public function paginate($filter);

    /**
     * @param  TEntity  $entity
     * @return TEntity
     *
     * @throws TEntityNotFoundException
     */
    public function update($entity);
}
