<?php

declare(strict_types=1);

namespace PQuijano\LaravelBase\Mixins;

use Closure;
use DateTimeInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

final class StrMixin
{
    public function cacheName(): Closure
    {
        return function (?string ...$names): string {
            return Str::snake(implode('_', Collection::make($names)->filter(fn (?string $name) => ! is_null($name))->all()));
        };
    }

    public function dbForeignKeyName(): Closure
    {
        return function (string $tableName, string ...$columnNames): string {
            return Str::limit('fk_'.$tableName.'_'.implode('_', $columnNames), 64, '');
        };
    }

    public function dbIndexName(): Closure
    {
        return function (string $tableName, string ...$columnNames): string {
            return Str::limit('ix_'.$tableName.'_'.implode('_', $columnNames), 64, '');
        };
    }

    public function dbUniqueIndexName(): Closure
    {
        return function (string $tableName, string ...$columnNames): string {
            return Str::limit('ak_'.$tableName.'_'.implode('_', $columnNames), 64, '');
        };
    }

    public function dbWildcard(): Closure
    {
        return function (?string $value): string {
            if (is_null($value)) {
                return null;
            }
            $newValue = trim($value);
            if (empty($newValue)) {
                return '';
            }
            $newValue = Str::replaceMatches('/[^A-Za-z0-9]+/', '%', $newValue);
            if (! str($newValue)->substr(0, 1)->exactly('%')) {
                $newValue = '%'.$newValue;
            }
            if (! str($newValue)->substr(-1, 1)->exactly('%')) {
                $newValue = $newValue.'%';
            }

            return $newValue;
        };
    }

    public function pathCombine(): Closure
    {
        return function (string ...$paths): string {
            return implode('/', $paths);
        };
    }

    public function ulidString(): Closure
    {
        return function (?DateTimeInterface $time = null): string {
            $ulid = Str::ulid($time);

            return Str::lower($ulid->__toString());
        };
    }
}
