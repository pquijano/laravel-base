# Laravel CQRS
Provies some Laravel 10 abstracts and string mixins.

## Installation
```shell
composer require pquijano/laravel-base:^1.0
```

## Usage
### String Mixins
```php
<?php

declare(strict_types=1);

use Illuminate\Support\Str;

$cacheName = Str::cacheName('test', 'cache', 'name'); // generates test_cache_name
$foreignKeyName = Str::dbForeignKeyName('table_name', 'column_name'); // generates kk_table_name_column_name
$indexName = Str::dbIndexName('table_name', 'column_name'); // generates ix_table_name_column_name
$uniqueName = Str::dbUniqueIndexName('table_name', 'column_name'); // generates ak_table_name_column_name
$wildCard = Str::dbWildcard('value'); // generates %value%
$path = Str::pathCombine('path', 'to', 'file'); // generates /path/to/file
$ulid = Str::ulidString(); // generates ulid string in small characters
```

### Abstracts
```php
<?php

declare(strict_types=1);

namespace App\Models;

use PQuijano\LaravelBase\Abstracts\Models\Model as AbstractModel;

final class Foo extends AbstractModel
{
    //
}
```
```php
<?php

declare(strict_types=1);

namespace App\Models;

use PQuijano\LaravelBase\Abstracts\Models\Pivot as AbstractPivot;

final class Foo extends AbstractPivot
{
    //
}
```
```php
<?php

declare(strict_types=1);

namespace App\Models;

use PQuijano\LaravelBase\Abstracts\Models\PersonalAccessToken as AbstractPersonalAccessToken;

final class PersonalAccessToken extends AbstractPersonalAccessToken
{
    //
}
```
```php
<?php

declare(strict_types=1);

namespace App\Models;

use PQuijano\LaravelBase\Abstracts\Models\User as AbstractUser;

final class User extends AbstractUser
{
    //
}
```
```php
<?php

declare(strict_types=1);

namespace App\Exceptions\Entities;

use PQuijano\LaravelBase\Abstracts\Exceptions\Entities\EntityNotFoundException as AbstractEntityNotFoundException;

final class UserNotFoundException extends AbstractEntityNotFoundException
{
    //
}
```
```php
<?php

declare(strict_types=1);

namespace App\Data\Repositories;

use PQuijano\LaravelBase\Abstracts\Data\Repositories\FilterGetData as AbstractFilterGetData;

final class FilterGetData extends AbstractFilterGetData
{
    //
}
```
```php
<?php

declare(strict_types=1);

namespace App\Data\Repositories;

use PQuijano\LaravelBase\Abstracts\Data\Repositories\FilterPaginateData as AbstractFilterPaginateData;

final class FilterPaginateData extends AbstractFilterPaginateData
{
    //
}
```

### Interfaces
```php
<?php

declare(strict_types=1);

namespace App\Interfaces\Repositories;

use PQuijano\LaravelBase\Interfaces\Repositories\Repository as RepositoryInterface;

interface UserRepository extends RepositoryInterface
{
    //
}
